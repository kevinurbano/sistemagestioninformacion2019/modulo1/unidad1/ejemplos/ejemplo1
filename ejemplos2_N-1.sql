﻿DROP DATABASE IF EXISTS m1ud1ejem2;

CREATE DATABASE IF NOT EXISTS m1ud1ejem2;

USE m1ud1ejem2;
/* N - 1 */
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  peso float,
  clienteId int,
  PRIMARY KEY (id),
  CONSTRAINT fkProductoCliente FOREIGN KEY (id)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  PRIMARY KEY(id)
);
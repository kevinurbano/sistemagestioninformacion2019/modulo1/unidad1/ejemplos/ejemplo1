﻿DROP DATABASE IF EXISTS m1ud1ejemplo1_ej5;

CREATE DATABASE IF NOT EXISTS m1ud1ejemplo1_ej5;

USE m1ud1ejemplo1_ej5;
/* (N - N)  -  (Multivaluado en compran y clientes) */

CREATE OR REPLACE TABLE productos(
  id int,
  nombre varchar(20),
  peso float,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int,
  nombre varchar(20),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE telefonos(
  cliente int,
  telefono int,
  PRIMARY KEY(cliente,telefono),
  CONSTRAINT fkTelefonosCliente FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  PRIMARY KEY(producto,cliente),
  CONSTRAINT fkCompranProductos FOREIGN KEY (producto)
    REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkCompranClientes FOREIGN KEY (cliente)
    REFERENCES clientes(id) on DELETE CASCADE on UPDATE CASCADE  
);

CREATE OR REPLACE TABLE compranDatos(
  producto int,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(producto,cliente,fecha)
);

/*  Restricciones  */

/* CompranDatos */
ALTER TABLE compranDatos 
  ADD CONSTRAINT fkcompranDatosCompran FOREIGN KEY(producto,cliente)
  REFERENCES compran(producto,cliente);
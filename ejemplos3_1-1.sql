﻿DROP DATABASE IF EXISTS m1ud1ejemplo1_ej3;

CREATE DATABASE IF NOT EXISTS m1ud1ejemplo1_ej3;

USE m1ud1ejemplo1_ej3;

 /* 1 - 1 */
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  peso float,
  clienteId int,
  fecha date,
  cantidad int,
  PRIMARY KEY(id),
  CONSTRAINT ukProductosClientes UNIQUE KEY (clienteId),
  CONSTRAINT fkProductosClientes FOREIGN KEY(clienteId)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  PRIMARY KEY(id)
);
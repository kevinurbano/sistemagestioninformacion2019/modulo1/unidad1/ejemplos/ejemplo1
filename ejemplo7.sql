﻿DROP DATABASE IF EXISTS m1ud1ejemplo1_ej7;

CREATE DATABASE IF NOT EXISTS m1ud1ejemplo1_ej7;

USE m1ud1ejemplo1_ej7;

CREATE OR REPLACE TABLE productos(
  id int,
  nombre varchar(20),
  peso float,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE clientes(
  id int,
  nombre varchar(20),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE telefonos(
  cliente int,
  telefono int,
  PRIMARY KEY(cliente,telefono),
  CONSTRAINT fkTelefonosCliente FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE tienda(
  codigo int,
  direccion varchar(30),
  PRIMARY KEY(codigo)
);

CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  tienda int,
  PRIMARY KEY(producto,cliente,tienda),
  UNIQUE KEY (producto,tienda),
  CONSTRAINT fkCompranProductos FOREIGN KEY (producto)
    REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkCompranClientes FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkCompranTienda FOREIGN KEY (tienda)
    REFERENCES tienda(codigo) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE OR REPLACE TABLE compranDatos(
  producto int,
  cliente int,
  tienda int,
  fecha date,
  cantidad int,
  PRIMARY KEY (producto,cliente,tienda,fecha)
);

/* Restricciones */
ALTER TABLE compranDatos 
  ADD CONSTRAINT fkCompranDatosCompran FOREIGN KEY (producto,cliente,tienda)
  REFERENCES compran(producto,cliente,tienda) ON DELETE CASCADE ON UPDATE CASCADE;
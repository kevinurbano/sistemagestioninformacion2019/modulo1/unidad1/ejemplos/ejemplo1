﻿DROP DATABASE IF EXISTS m1ud1ejemplo1_ej4;

CREATE DATABASE IF NOT EXISTS m1ud1ejemplo1_ej4;

USE m1ud1ejemplo1_ej4;
/* 
  (N - N)  (Multivaluado telefono)
*/
 
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  peso float,
  PRIMARY KEY (id)
);

CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE telefonos(
  cliente int,
  telefono varchar(12),
  PRIMARY KEY(cliente,telefono)
);

CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(producto,cliente)
);

/* Creacion de las restricciones */
/* Compran */
ALTER TABLE compran 
  ADD CONSTRAINT fkCompranProducto FOREIGN KEY (producto) 
    REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fkCompranCliente FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;

/* Telefono */
ALTER TABLE telefonos
  ADD CONSTRAINT fkTelefonosClientes FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;
﻿DROP DATABASE IF EXISTS m1ud1ejemplo1_ej1;

CREATE DATABASE IF NOT EXISTS m1ud1ejemplo1_ej1;

USE m1ud1ejemplo1_ej1;
/* N - N */

 -- Creacion de la tabla productos
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  peso int,
  PRIMARY KEY (id)
);

-- Creacion de la tabla clientes
CREATE OR REPLACE TABLE clientes(
  id int AUTO_INCREMENT,
  nombre varchar(30),
  PRIMARY KEY(id)
);

/*
CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(producto,cliente),
  CONSTRAINT fkCompranProducto FOREIGN KEY (producto)
    REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkCompranCliente FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE
);
*/


-- Creacion de la tabla compran
CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(producto,cliente)  
);

/** Fin de la creación de las tablas **/


/* Creacion de las restricciones */
/* Compran */
ALTER TABLE compran 
  ADD CONSTRAINT fkCompranProducto FOREIGN KEY (producto) 
    REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD  CONSTRAINT fkCompranCliente FOREIGN KEY (cliente)
    REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;


/* Introducción de datos */
/* Tabla Productos */
INSERT INTO productos (id, nombre, peso) VALUES
(1, 'PRODUCTO1', 2),
(2, 'PRODUCTO2', 3),
(3, 'PRODUCTO3', 4),
(4, 'PRODUCTO4', 5),
(5, 'PRODUCTO5', 6),
(6, 'PRODUCTO6', 7),
(7, 'PRODUCTO7', 8),
(8, 'PRODUCTO8', 9),
(9, 'PRODUCTO9', 10),
(10, 'PRODUCTO10', 11),
(11, 'PRODUCTO11', 12),
(12, 'PRODUCTO12', 13),
(13, 'PRODUCTO13', 14),
(14, 'PRODUCTO14', 15),
(15, 'PRODUCTO15', 16),
(16, 'PRODUCTO16', 17),
(17, 'PRODUCTO17', 18),
(18, 'PRODUCTO18', 19),
(19, 'PRODUCTO19', 20);

/* Tabla Clientes */
INSERT INTO clientes VALUES
(1, 'CLIENTE1'),
(2, 'CLIENTE2'),
(3, 'CLIENTE3'),
(4, 'CLIENTE4'),
(5, 'CLIENTE5'),
(6, 'CLIENTE6'),
(7, 'CLIENTE7'),
(8, 'CLIENTE8'),
(9, 'CLIENTE9'),
(10, 'CLIENTE10'),
(11, 'CLIENTE11'),
(12, 'CLIENTE12'),
(13, 'CLIENTE13'),
(14, 'CLIENTE14'),
(15, 'CLIENTE15'),
(16, 'CLIENTE16'),
(17, 'CLIENTE17'),
(18, 'CLIENTE18'),
(19, 'CLIENTE19'),
(20, 'CLIENTE20');

/* Tabla Compran */
INSERT INTO compran VALUES
(1, 18, '2019-06-18 00:00:00', NULL),
(1, 19, '2019-06-19 00:00:00', NULL),
(2, 18, '2019-06-20 00:00:00', NULL),
(3, 1, '2019-06-13 00:00:00', NULL);